# Docus

Documentos compartidos y en curso de proyectos de [Interferencias](interferencias.tech)

## JASYP2022

Como en Interferencias priorizamos la transparencia, hemos decidido crear este documento para compartir nuestro avance en la creación del evento, siendo accesible a la lectura y por supuesto aceptamos revisión de propuestas a través de Merge Request.

**¿Qué son las JASYP?***

Jornadas de anonimato, seguridad y privacidad, un evento activo desde 2017 de la mano de interferencias que pretende juntar proyectos y personas centrados en esas temáticas. Hasta ahora se había hecho una edición al año en Granada, sin embargo por razones de salud nos vimos obligadas a atrasar las jornadas. En 2022, queremos volver con más fuerza.

**¿Puedo mandar el C4P por aquí ya?**

¡Aún no! Este repositorio solo es una idea de transparencia sobre notas de organización, Código de Conducta y otros documentos.

## Fuera Google de las Aulas

Desde el inicio de la pandemia e incluso antes hemos visto como en colegios y otras instituciones educativas públicas los recursos digitales se están centralizando y volviendo cada vez más dependientes de Google y Microsoft. Desde Interferencias queremos aportar ideas libres, seguras y que respeten la privacidad para todas las etapas educativas. Aún estamos trabajando en ello, aprendiendo y creando contenido, hemos creado este repo con la idea de compartir ese avance, también. Todo lo que discutamos y aprendamos al respecto, pretende ser volcado en este repositorio, y aceptamos propuestas ajenas.

